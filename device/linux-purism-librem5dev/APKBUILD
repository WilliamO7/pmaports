# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/librem5-evk_defconfig

pkgname="linux-purism-librem5dev"
pkgver=4.18.11
pkgrel=3
pkgdesc="Purism Librem 5 devkit kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5dev"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_repository="linux-emcraft"
_commit="bcc9a4658624009ce94abeb19b1be07b55d6da21"
_config="config-${_flavor}.${arch}"
source="
	$pkgname-$_commit.tar.gz::https://source.puri.sm/Librem5/${_repository}/-/archive/${_commit}.tar.gz
	$_config
"
builddir="$srcdir/${_repository}-${_commit}"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		downstreamkernel_prepare "$srcdir" "$builddir" "$_config" "$_carch" "$HOSTCC"
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	# kernel.release
	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# zImage (find the right one)
	cd "$builddir/arch/$_carch/boot"
	_target="$pkgdir/boot/vmlinuz-$_flavor"
	for _zimg in zImage-dtb Image.gz-dtb *zImage Image; do
		[ -e "$_zimg" ] || continue
		msg "zImage found: $_zimg"
		install -Dm644 "$_zimg" "$_target"
		break
	done
	if ! [ -e "$_target" ]; then
		error "Could not find zImage in $PWD!"
		return 1
	fi
	cd "$builddir"
	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}

sha512sums="51fd07402e63ce7538e65c01deba51faaf60cbb853d0af2d362aceb710e608d0cdee4b53524d32b4217b7da7cc827270c427207e7bd34b8b5f87b43791aa81db  linux-purism-librem5dev-bcc9a4658624009ce94abeb19b1be07b55d6da21.tar.gz
15e2143ee6a46ff57f4464c34d7aac58b46d513955866ba68061bb41d597b9edd1ef2e62e2e1f7224a2cc220e59f39ec75608890552b5dfe23b6def512a09772  config-purism-librem5dev.aarch64"
